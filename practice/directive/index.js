import Vue from 'vue'

new Vue({
    el: '#root',
    // v-text 数据绑定 不建议使用
    // v-html 引用html代码执行
    // v-show 是否显示 相当于加上 display: none  节点代码还是在的
    // v-if 可以作为是否显示 true显示 false不显示 节点代码直接注释掉<!---->
    template: `
        <div>
            <div v-show="active">show: {{text}}</div>
            <!-- 不解析数据绑定 -->
            <div v-pre>pre: {{text}}</div>
            <!-- 不会用到了 -->
            <div v-cloak>cloak: {{text}}</div>
            <!-- 数据绑定的内容只执行一次 -->
            <div v-once>once: {{text}}</div>
            <div v-if="active">if: {{text}}</div>
            <div v-else-if="text === 0">else-if: {{text}}</div>
            <div v-else>else content</div>
            <div v-html="html"></div>
            <!-- 可以加修饰符 number 转换为数值 测试但是第一个输入英文或中文还是字符串-->
            <input type="text" v-model.number="text">
            <!-- 可以加修饰符 去除两边的空格-->
            <input type="text" v-model.trim="text2">
            </br>
            <input type="checkbox" v-model="active">
            <div>
                <input type="checkbox" :value="1" v-model="arr">
                <input type="checkbox" :value="2" v-model="arr">
                <input type="checkbox" :value="3" v-model="arr">
            </div>
            <div>
                <input type="radio" value="one" v-model="picked">
                <input type="radio" value="two" v-model="picked">
            </div>
            <ul>
                <li v-for="(item, index) in arr" :key="item">{{item}} : {{index}}</li>
            </ul>
            <ul>
                <li v-for="(val, key, index) in obj">{{key}} : {{val}} : {{index}}</li>
            </ul>
        </div>
    `,
    data: {
        arr: [1, 2, 3],
        obj: {
            a: 123,
            b: 456,
            c: 789
        },
        picked: '',
        text: 0,
        text2: 0,
        active: false,
        html: '<span>this is html</span>'
    }
})
