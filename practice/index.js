// 如果不加上 index.js 会自动加载目录下的 index.js

// import './instance/index.js' // 第一节课 Vue实例
// import './lifecycle' // 第二节课 Vue的生命周期方法
// import './data-binding' // 第三节课 Vue的数据绑定
// import './computed' // 第四节课 computed和watch使用场景和方法
// import './directive' // 第五节课 Vue的原生指令
// import './component/define' // 第六节课 Vue的组件之组件的定义
// import './component/extend' // 第七节课 Vue的组件之组件的继承
// import './component/v-model' // 第八节课 Vue的组件之自定义双向绑定
// import './component/features' // 第九节课 Vue的组件之高级属性
import './component/render' // 第十节课 Vue的组件之高级属性
