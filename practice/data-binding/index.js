import Vue from 'vue'

new Vue({
    el: '#root',
    // template: "<div>{{isActive ? 'active' : 'not active'}}</div>",
    // template: "<div>{{arr.join(' ')}}</div>",

    // 简写 bind: = : , v-on: = @
    // template: "<div :id='aaa' @click='handleClick'><p v-html='html'></p></div>",

    // 对象判断写法
    // template: "<div :class='{ active: isActive}'><p v-html='html'></p></div>",
    // 数组判断写法 三元运算写法
    // 加点的 `<div></div>` 里面定义都是字符串
    // template: `<div :class="[isActive ? 'active' : '']">
    //     <p v-html="html"></p>
    // </div>`,
    // 混合式写法 数组+对象
    template: `<div :class="[{ active: isActive}]" :style="[styles, styles2]">
        <p v-html="html"></p>
    </div>`,
    data: {
        isActive: false,
        arr: [1, 2, 3],
        html: '<span>123</span>',
        aaa: 'main',
        styles: {
            color: 'red',
            // appearance css属性是 消除浏览器默认样式
            // 这个属性还需要加前缀 vue会自动加上
            appearance: 'none'
        },
        // 以后面的为主 替代前面的
        styles2: {
            color: 'blue'
        }
    },
    computed: {
        // 也可以根据这个绑定class
        classNames () {

        }
    },
    methods: {
        handleClick () {
            alert('clicked')
        },
        getJoinedArr (arr) {
            return arr.join(' ')
        }
    }
})
