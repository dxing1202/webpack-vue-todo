import Vue from 'vue'

new Vue({
    el: '#root',
    // <span>Name: {{firstName + ' ' + lastName}}</span> // 普通样式 直接连接
    template: `
        <div>
            <p>Name: {{name}}</p>
            <p>Name: {{getName()}}</p>
            <p>Number: {{number}}</p>
            <p>fullName: {{fullName}}</p>
            <p><input type="text" v-model="number"></p>
            <p>firstName: <input type="text" v-model="firstName"></p>
            <p>lastName: <input type="text" v-model="lastName"></p>
            <p>Name: <input type="text" v-model="name"></p>
            <p>obj.a: <input type="text" v-model="obj.a"></p>
        </div>
    `,
    data: {
        firstName: 'Jokcy',
        lastName: 'Lou',
        number: 0,
        fullName: '',
        obj: {
            // a: '123'
            a: 0
        }
    },
    computed: {
        // name() {
        //     console.log('new name')
        //     return `${this.firstName} ${this.lastName}`
        // },
        // computed 也可以做重新设置的事 ，但是不建议
        name: {
            get() {
                console.log('new Name')
                return `${this.firstName} ${this.lastName}`
            },
            // 这样的设置很复杂，不到万不得已的情况都不要用 set方法
            set(name) {
                const names = name.split(' ')
                this.firstName = names[0]
                this.lastName = names[1]
            }
        }
    },
    mounted () {
        // this.obj = {
        //     a: '345'
        // }
    },
    watch: {
        // firstName (newName, oldName) {
        //     this.fullName = newName + ' ' + this.lastName
        // }
        // firstName: {
        //     handler (newName, oldName) {
        //         this.fullName = newName + ' ' + this.lastName
        //     },
        //     // 立即执行一次 默认false
        //     immediate: true,
        //     // 默认false
        //     deep: true
        // },

        // 直接指定的是obj是个对象，无法进行修改
        // 指定的是 obj.a 的属性就可以直接执行
        'obj.a': {
            handler () {
                console.log(`obj.a changed`)
                // 不要在watch 和 computed 里面进行修改值
                // this.obj.a += 1
            },
            // 立即执行一次 默认false
            immediate: true,
            // 默认false
            // deep: true
        }
    },
    methods: {
        getName () {
            console.log('getName invoked')
            return `${this.firstName} ${this.lastName}`
        }
    }
})
