import Vue from 'vue'

// const div = document.createElement('div')
// document.body.appendChild(div)

const app = new Vue({
    // el: '#root',
    template: '<div ref="div">{{text}}  {{obj.a}}</div>',
    data: {
        text: 0,
        obj: {}
    },
    // watch: {
    //     text (newText, oldText) {
    //         console.log(`${newText} : ${oldText}`)
    //     }
    // }
})

app.$mount('#root')

// app.text = 'text1'

// let i = 0
setInterval( () => {
    app.text += 1
    // app.text += 1
    // app.text += 1
    // app.text += 1
    // app.text += 1

    // i++
    // app.obj.a = i // 这样直接赋值，不会有效，需要用$forceUpdate发现重新渲染
    // app.$forceUpdate()
    // app.$set(app.obj, 'a', i) // 使用set方法设值可以直接有效
    // app.$delete

    // app.$options.data.text += 1 // 这样的方法修改无法以以上的data.text想通
    // app.$data.text += 1 // 这样的方法可行
},1000)

// console.log(app.$data)
// console.log(app.$props)
// console.log(app.$el)
// console.log(app.$options)

// 直接给$options.render赋值是生效的，不过需要下一次有值变化重新渲染的时候才会生效
// app.$options.render = (h) => {
//     return h('div', {}, 'new render function')
// }
// console.log(app.$root === app)

// <item><div></div></item> 在组件的时候需要用
// console.log(app.$children)

// 讲插槽的时候用到
// console.log(app.$slots)
// console.log(app.$scopedSlots)

// 一个对象，持有注册过 ref 特性 的所有 DOM 元素和组件实例。
// 会返回html ref=xxx 的节点对象
// console.log(app.$refs)

// 服务端渲染的才时候用到
// console.log(app.$isServer)

// const unWatch = app.$watch('text', (newText, oldText) => {
//     console.log(`${newText} : ${oldText}`)
// })
// setTimeout( () => {
//     unWatch();
// }, 2000)

// $on 事件可以触发很多次 $once 事件只可以触发一次
// app.$once('test', (a, b) => {
//     console.log(`test emited ${1} ${b}`)
// })
// app.$emit('test', 1, 2)
// setInterval( () => {
//     app.$emit('test', 1, 2)
// }, 1000)

// 强制主键重新渲染一次
// app.$forceUpdate()
