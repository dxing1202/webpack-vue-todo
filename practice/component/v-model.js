import Vue from 'vue'

const component = {
    model: {
        // 修改双向绑定的属性是 value1
        prop: 'value1',
        event: 'change' // 自定义事件名称
    },
    props: {
        value: String,
        value1: String
    },
    template: `
        <div>
            <input type="text" @input="handleInput" :value="value1"/>
        </div>
    `,
    methods: {
        handleInput (e) {
            // this.$emit('input', e.target.value)
            // 绑定事件 change 默认是input
            this.$emit('change', e.target.value)
        }
    }
}

new Vue({
    components: {
        CompOne: component
    },
    el: '#root',
    data () {
        return {
            value: ''
        }
    },
    // 向组件设置v-model
    // <comp-one :value="value" @input="value = arguments[0]" v-model="value">
    template: `
        <div>
            <comp-one v-model="value"></comp-one>
        </div>
    `
})
