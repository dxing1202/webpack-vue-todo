import Vue from 'vue'

const component = {
    // 这样写也是可以的，但是不严谨，不推荐
    // props: ['active', 'propOne']
    props: {
        // active: Boolean,
        active: {
            type: Boolean, // 属性数据类型
            required: true, // 必须传这个属性
            default: true, // 默认值 这个跟required不会同时使用，用default再用required没有意义
            validator (value) {
                // typeof 操作符来检测变量的数据类型
                return typeof value === 'boolean'
            }
        },
        propOne: String,
        // onChange: Function // 使用 $emit 传函数可以不用这一步
    },
    template: `
        <div>
            <input type="text" v-model="text" />
            <span @click="handleChange">{{propOne}}</span>
            <span v-show="active">see me if active</span>
        </div>
    `,
    data () {
        return {
            text: 0
        }
    },
    methods: {
        handleChange () {
            // this.onChange()
            this.$emit('change')
        }
    }
}

// Vue.component('CompOne', component)

new Vue({
    components: {
        CompOne: component
    },
    data: {
        prop1: 'text1'
    },
    methods: {
        handleChange () {
            this.prop1 += 1
        }
    },
    mounted () {
        // console.log(this.$refs.comp1) // 可以拿到实例
    },
    el: '#root',
    // template: `<div>123</div>`
    template: `
        <div>
            <comp-one ref="comp1" :active="true" :prop-one="prop1" @change="handleChange"></comp-one>
            <comp-one :active="false" prop-one="text2"></comp-one>
        </div>
    `
    // :on-change="handleChange // 上面组件用 this.onChange 就用这种写法
})
