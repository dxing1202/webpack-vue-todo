import Vue from 'vue'

const component = {
    props: {
        active: Boolean,
        propOne: String
    },
    template: `
        <div>
            <input type="text" v-model="text" />
            <span @click="handleChange">{{propOne}}</span>
            <span v-show="active">see me if active</span>
        </div>
    `,
    data () {
        return {
            text: 0
        }
    },
    mounted () {
        console.log('comp mounted')
    },
    methods: {
        handleChange () {
            // this.onChange()
            this.$emit('change')
        }
    }
}

const parent = new Vue({
    name: 'parent'
})

const component2 = {
    // parent: parent,
    extends: component,
    data () {
        return {
            text: 1
        }
    },
    mounted () {
        console.log('comp2 mounted'),
        // console.log(this.$parent.$options.name)
        // this.$parent.text = '12345'
        console.log(this.$parent.$options.name)
    }
}

// const CompVue = Vue.extend(component)

// new CompVue({
//     el: '#root',
//     // 组件的prop 需要使用 propsData传值才有效
//     propsData: {
//         propOne: 'xxx'
//     },
//     data: {
//         text: 123
//     },
//     mounted () {
//         console.log('intance mounted')
//     }
// })

new Vue({
    // 不建议使用parent属性 不建议修改父组件的东西
    parent: parent,
    name: 'Root',
    el: '#root',
    mounted () {
        console.log(this.$parent.$options.name)
    },
    components: {
        Comp: component2
    },
    data: {
        text: 23333
    },
    template: `
        <div>
            <span>{{text}}</span>
            <comp></comp>
        </div>
    `
})
