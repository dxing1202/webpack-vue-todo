import Vue from 'vue'

const ChildComponent = {
    // template: '<div>child component : {{value}}</div>',
    template: '<div>child component : {{data.value}}</div>',
    // inject: ['yeye','value'],
    inject: ['yeye','data'],
    mounted () {
        // console.log(this.$parent.$options.name)
        console.log(this.yeye, this.value)
    }
}

const component = {
    name: 'comp',
    components: {
        ChildComponent
    },
    // 定义插槽 <slot></slot> 这样就可以在组件内部添加内容
    // 具名插槽 加上name属性
    template: `
        <div :style="style">
            <slot :value="value" aaa="111"></slot>
            <child-component />
        </div>
    `,
    // 具名插槽模版代码
    // <div class="header">
    //     <slot name="header"></slot>
    // </div>
    // <div class="body">
    //     <slot name="body"></slot>
    // </div>
    data () {
        return {
            style: {
                width: '200px',
                height: '200px',
                border: '1px solid #aaa'
            },
            value: 'component value'
        }
    }
}

new Vue({
    components: {
        CompOne: component
    },
    // Object|Function 要使用函数方式才能初始化完显示， 用起来跟Data一样
    provide () {
        const data = {}

        // 这样设置后 value属性值 才会根据变化而变化
        // 官方不建议这样使用， 可能更新版本后可能不行
        Object.defineProperty(data, 'value', {
            get: () => this.value, // 拿到 this.value
            enumerable: true
        })

        return {
            yeye: this,
            // value: this.value
            data
        }
    },
    el: '#root',
    data () {
        return {
            value: '123'
        }
    },
    mounted () {
        console.log(this.$refs.comp.value, this.$refs.span)
    },
    // 具名插槽代码
    // <span slot="header">this is header</span>
    // <span slot="body">this is body</span>

    // slot-scope 作用域插槽 props.value 使用组件内部的value值
    template: `
        <div>
            <comp-one ref="comp">
                <span slot-scope="props" ref="span">{{props.value}} {{props.aaa}} {{value}}</span>
            </comp-one>
            <input type="text" v-model="value">
        </div>
    `
})
