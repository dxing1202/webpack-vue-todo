import Vue from 'vue'

const app =  new Vue({
    // el: '#root',
    // template: '<div>{{text}}</div>',
    data: {
        text: 0
    },
    // 在实例初始化之后，数据观测 (data observer) 和 event/watcher 事件配置之前被调用。
    beforeCreate () {
        console.log(this, 'beforeCreate')
        console.log(this.$el, 'beforeCreate')
    },
    // 在实例创建完成后被立即调用。在这一步，实例已完成以下的配置：数据观测 (data observer)，属性和方法的运算，watch/event 事件回调。然而，挂载阶段还没开始，$el 属性目前尚不可用。
    created () {
        console.log(this, 'created')
        console.log(this.$el, 'created')
    },
    // 在挂载开始之前被调用：相关的 render 函数首次被调用。
    beforeMount () {
        console.log(this, 'beforeMount')
        console.log(this.$el, 'beforeMount')
    },
    // 实例被挂载后调用，这时 el 被新创建的 vm.$el 替换了。 如果根实例挂载到了一个文档内的元素上，当mounted被调用时vm.$el也在文档内。
    mounted () {
        console.log(this, 'mounted')
        console.log(this.$el, 'mounted')
    },
    // 下面的就自己去vue官方查文档 API页面
    beforeUpdate () {
        console.log(this, 'beforeUpdate')
    },
    updated () {
        console.log(this, 'updated')
    },
    // 在组件章节讲解
    activated () {
        console.log(this, 'activated')
    },
    // 在组件章节讲解
    deactivated () {
        console.log(this, 'deactivated')
    },
    beforeDestroy () {
        console.log(this, 'beforeDestroy')
    },
    destroyed () {
        console.log(this, 'destroyed')
    },
    render (h) {
        // 主动触发一个错误， 触发renderError属性
        // throw new TypeError('render error')

        console.log('render function invoked')
        // 内容跟上面的template一样
        return h('div', {}, this.text)
    },
    renderError (h, err) {
        return h('div', {}, err.stack)
    },
    errorCaptured () {
        // 会向上冒泡，并且正式环境可以使用
    }
})

app.$mount('#root')

// setInterval( () => {
//     app.text +=1
// },1000)

// setTimeout( () => {
//     // $destroy 完全销毁一个实例。清理它与其它实例的连接，解绑它的全部指令及事件监听器。
//     app.$destroy()
// },1000)
