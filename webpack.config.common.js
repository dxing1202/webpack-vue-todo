const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
// 清理 /dist 文件夹
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const createVueLoaderOptions = require('./vue-loader.config');

const isDev = process.env.NODE_ENV === 'development';

module.exports = {
    target: "web", // 默认也是web
    entry: path.join(__dirname, 'src/index.js'),
    output: {
      // 开发环境 可以随意的设置输出文件 或者加上哈希
      // filename: 'bundle.js',
      filename: 'bundle.[hash:8].js',
      path: path.resolve(__dirname, 'dist'),
      // publicPath: '/public/'
    },
    plugins :[
        new CleanWebpackPlugin(), // 清理 /dist 文件夹 // 可以只放到生产环境
        new VueLoaderPlugin(), // 使用 VueLoaderPlugin 插件
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: isDev ? '"development"' : '"production"' // 必须要有引号
            }
        }),
        // 使用 HtmlWebpackPlugin 插件
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'template.html')
        })
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: createVueLoaderOptions(isDev)
            },
            {
                test: /\.jsx$/,
                loader: 'babel-loader'
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                '@babel/preset-env'
                            ]
                        }
                    }
                ]
            },
            // {
            //     test: /\.css$/,
            //     use: [
            //         'vue-style-loader',
            //         {
            //             loader: 'css-loader',
            //             options: {
            //               // 开启 CSS Modules
            //               modules: true,
            //               // 自定义生成的类名
            //               // localIdentName: '[local]_[hash:base64:8]'
            //               localIdentName: isDev ? '[local]-[path]-[name]-[hash:base64:5]' : '[hash:base64:5]'
            //             }
            //          }
            //     ]
            // },
            {
                test: /\.(gif|jpg|jpeg|png|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 1024,
                            name: 'resources/[path][name].[hash:8].[ext]'
                        }
                    }
                ]
            }
        ]
    }
}
