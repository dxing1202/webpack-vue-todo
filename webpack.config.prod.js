const merge = require('webpack-merge');
const common = require('./webpack.config.common');
const path = require('path');

// 该插件将CSS提取到单独的文件中。它为每个包含CSS的JS文件创建一个CSS文件。它支持CSS和SourceMap的按需加载。
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// 优化js 使用优化的插件
const TerserJSPlugin = require('terser-webpack-plugin');
// 优化css 提取出来后的单独css文件
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    entry: {
        app: path.join(__dirname, 'src/index.js')
        // vendor: ['vue']
    },
    output: {
        filename: '[name].[chunkhash:8].js'
    },
    plugins: [
        // 使用这个最新的打包单独css插件 MiniCssExtractPlugin
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // all options are optional
            // filename: '[name].css',
            filename: '[name].[contentHash:8].css',
            chunkFilename: '[id].[contentHash:8].css',
            // ignoreOrder: false, // Enable to remove warnings about conflicting order
        })
    ],
    optimization: {
        splitChunks: {
            chunks: "all",
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        },
        runtimeChunk: true,
        minimizer: [
            // 优化js文件
            new TerserJSPlugin({}),
            // 优化css文件
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    module: {
        rules: [
            {
                test: /\.styl/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    'stylus-loader'
                ]
            }
        ]
    }

});
