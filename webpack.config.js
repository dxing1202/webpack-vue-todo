const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 清理 /dist 文件夹
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 这个插件暂时不用
// const ExtractTextPlugin = require("extract-text-webpack-plugin");
// 使用这个插件代替上面的那个使用
// 该插件将CSS提取到单独的文件中。它为每个包含CSS的JS文件创建一个CSS文件。它支持CSS和SourceMap的按需加载。
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// 优化js 使用优化的插件
const TerserJSPlugin = require('terser-webpack-plugin');
// 优化css 提取出来后的单独css文件
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';

const config = {
  target: "web", // 默认也是web
  entry: path.join(__dirname, 'src/index.js'),
  output: {
    // 开发环境 可以随意的设置输出文件 或者加上哈希
    // filename: 'bundle.js',
    filename: 'bundle.[hash:8].js',
    path: path.join(__dirname, 'dist')
  },
  optimization: {
      splitChunks: {
          chunks: "all",
          minSize: 30000,
          minChunks: 1,
          maxAsyncRequests: 5,
          maxInitialRequests: 3,
          automaticNameDelimiter: '~',
          name: true,
          cacheGroups: {
              vendors: {
                  test: /[\\/]node_modules[\\/]/,
                  priority: -10
              },
              default: {
                  minChunks: 2,
                  priority: -20,
                  reuseExistingChunk: true
              }
          }
      },
      runtimeChunk: true,
      minimizer: []
  },
  // resolve: {
  //     alias: {
  //         'vue': path.resolve(__dirname, 'node_modules/vue/dist/vue.js')
  //     }
  // },
  mode: isDev ? 'development' : 'production', // development || production
  plugins :[
      new VueLoaderPlugin(), //new一个实例
      new webpack.DefinePlugin({
          'process.env': {
              NODE_ENV: isDev ? '"development"' : '"production"' // 必须要有引号
          }
      }),
      new HtmlWebpackPlugin(),
      new CleanWebpackPlugin() // 清理 /dist 文件夹 // 可以只放到生产环境
  ],
  module: {
      rules: [
          {
              test: /\.vue$/,
              loader: 'vue-loader'
          },
          {
              test: /\.jsx/,
              loader: 'babel-loader'
          },
          // 因为项目中没有用到css文件，可以先注释掉。
          // {
          //     test: /\.css$/,
          //     use: [
          //         'style-loader',
          //         'css-loader'
          //     ]
          // },
          // 根据不同的环境进行配置
          // {
          //     test: /\.styl/,
          //     use: [
          //         'style-loader',
          //         'css-loader',
          //         {
          //             loader: 'postcss-loader',
          //             options: {
          //                 sourceMap: true
          //             }
          //         },
          //         'stylus-loader'
          //     ]
          // },
          {
              test: /\.(gif|jpg|jpeg|png|svg)$/,
              use: [
                  {
                      loader: 'url-loader',
                      options: {
                          limit: 1024,
                          name: '[name]-dxing1202.[ext]'
                      }
                  }
              ]
          }
      ]
  }
};

if( isDev ){
    // 这里是开发环境配置
    config.module.rules.push(
        {
            test: /\.styl/,
            use: [
                'style-loader',
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true
                    }
                },
                'stylus-loader'
            ]
        }
    );
    config.devtool = '#cheap-module-eval-source-map'
    config.devServer = {
        port: 8000, // 设置端口
        host: '127.0.0.1', // 设置0.0.0.0 不能运行，换回127.0.0.1
        overlay: {
            errors: true // 提示错误
        },
        open: false, // 启动 webpack-dev-server 会自动打开浏览器
        hot: true // 修改资源文件不会刷新页面更新，直接更新数据
    }
    // config.plugins.push 直接在原来的config.plugins 加入 push加入的意思
    config.plugins.push(
        // 模块热替换插件(HotModuleReplacementPlugin) 也被称为 HMR
        // 永远不要在生产环境(production)下启用 HMR
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    )
} else {
    // 这里是生产环境配置

    config.entry = {
        app: path.join(__dirname, 'src/index.js'),
        // vendor: ['vue','vue-loader']
        vendor: ['vue']
    }
    // 生产环境 输出文件 命名
    config.output.filename = '[name].[chunkhash:8].js';
    // config.optimization.minimizer = [
    //     new TerserJSPlugin({}),
    //     new OptimizeCSSAssetsPlugin({})
    // ];
    config.optimization.minimizer.push(
        // 优化js文件
        new TerserJSPlugin({}),
        // 优化css文件
        new OptimizeCSSAssetsPlugin({})
    );
    config.plugins.push(
        // 这个把css文件单独出来的插件不用了， webpack4以上独立出来一个专用css
        // new ExtractTextPlugin('styles.[contentHash:8].css')

        // 使用这个最新的打包单独css插件 MiniCssExtractPlugin
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // all options are optional
            // filename: '[name].css',
            filename: '[name].[contentHash:8].css',
            chunkFilename: '[id].[contentHash:8].css',
            // ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        // 该插件CommonsChunkPlugin webpack4 已弃用
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendor'
        // });
        // SplitChunksPlugin 代替 CommonsChunkPlugin
        // config.optimization.splitChunks = {
        //     name: 'vendor'
        // }
    );
    config.module.rules.push(
        // 这个把css文件单独出来的插件路由不用了， webpack4以上独立出来一个专用css
        // {
        //     test: /\.styl/,
        //     use: ExtractTextPlugin.extract({
        //         fallback: 'style-loader',
        //         use: [
        //             'css-loader',
        //             {
        //                 loader: 'postcss-loader',
        //                 options: {
        //                     sourceMap: true
        //                 }
        //             },
        //             'stylus-loader'
        //         ]
        //     })
        // }

        //  MiniCssExtractPlugin 使用配置路由
        {
            test: /\.styl/,
            use: [
                // {
                //     loader: MiniCssExtractPlugin.loader,
                //     options: {
                //         // you can specify a publicPath here
                //         // by default it uses publicPath in webpackOptions.output
                //         publicPath: './dist',
                //         // hmr: process.env.NODE_ENV === 'development',
                //     }
                // },
                MiniCssExtractPlugin.loader,
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true
                    }
                },
                'stylus-loader'
            ]
        }
    );
}

module.exports = config;
