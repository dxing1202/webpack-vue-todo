// const docsLoader = require.resolve('./doc-loader');

// () => {}
// 这样相当是一个函数， 后面return回来就是数值了
// 这样的写话好处是可以引入变量
module.exports = (isDev) => {
    return {
        // 控制代码多出来的空格 // 在vue-loader选项文档已经没有此选项
        // preserveWhitepace: true,
        // 新版本应该改成这样
        compilerOptions: {
            // 放弃模板标签之间的空格
            preserveWhitespace: false
        },

        // 按需求加载CSS // 在vue-loader选项文档已经没有此选项
        extractCSS: !isDev,

        // 在vue-loader选项文档已经没有此选项
        // 测试没用， 老师讲的应该是老版本了
        // 看完官方文档，直接在 rules中设置了
        // cssModules: {
        //     localIdentName: isDev ? '[path]-[name]-[hash:base64:5]' : '[hash:base64:5]',
        //     camelCase: true
        // },

        // postCss // 已经定义全局的配置 postcss.config.js

        // 热重载
        // 在开发环境下是 true，在生产环境下或 webpack 配置中有 target: 'node' 的时候是 false
        // 一般不用设置
        // hotReload: false

        // 老师说是比较高级的东西，常规来说不怎么用到，以后需要再查文档学习吧
        // loaders: {
        //     'docs': docsLoader // 引用上面引入的文件， 文件没有创建，老师也不给看到是什么文件
        // },
        // preLoader: {
        //
        // },
        // postLoader: {
        //
        // }
    }
}
