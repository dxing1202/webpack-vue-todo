const merge = require('webpack-merge');
const common = require('./webpack.config.common');
const path = require('path');
const webpack = require('webpack');

module.exports = merge(common, {
    mode: 'development',
    entry: path.join(__dirname, 'practice/index.js'),
    module: {
        rules: [
            {
                test: /\.styl(us)?$/,
                use: [
                    'vue-style-loader',
                    // 'style-loader',
                    'css-loader',
                    // {
                    //     loader: 'css-loader',
                    //     options: {
                    //       // 开启 CSS Modules
                    //       modules: true,
                    //       // 自定义生成的类名
                    //       localIdentName: '[local]_[hash:base64:5]'
                    //     }
                    // },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    'stylus-loader'
                ]
            }
        ]
    },
    devtool: '#cheap-module-eval-source-map',
    devServer: {
        port: 8000, // 设置端口
        host: '127.0.0.1', // 设置0.0.0.0 不能运行，换回127.0.0.1
        overlay: {
            errors: true // 提示错误
        },
        open: false, // 启动 webpack-dev-server 会自动打开浏览器
        hot: true // 修改资源文件不会刷新页面更新，直接更新数据
    },
    resolve: {
        alias: {
            'vue': path.join(__dirname, 'node_modules/vue/dist/vue.esm.js')
        }
    },
    plugins: [
        // 模块热替换插件(HotModuleReplacementPlugin) 也被称为 HMR
        // 永远不要在生产环境(production)下启用 HMR
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
        // new webpack.EnvironmentPlugin({
        //     NODE_ENV: 'development',
        //     DEBUG: false
        // });
    ]
});
