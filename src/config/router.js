import Router from 'vue-router'

import routes from './routes'

// const router = new Router({
//     routes
// })

// export default router
// 这样导入会导致服务端渲染的时候 内存溢出

export default () => {
    return new Router({
        routes,
        mode: 'history',
        // base: '/base/',
        // linkActiveClass: 'active-link',
        // linkExactActiveClass: 'exact-active-link',
        // 下面这个是滚动条的问题
        scrollBehavior (to, from, savedPosition) {
            if (savedPosition) {
                return savedPosition
            } else {
                return { x: 0, y: 0}
            }
        },
        fallback: true
        // parseQuery (query) {
        //     // 将url后缀的参数转化 默认自动转化
        //
        // },
        // stringifyQuery (obj) {
        //
        // },
    })
}
