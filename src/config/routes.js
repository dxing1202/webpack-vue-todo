// import Todo from '../views/todo/todo.vue'
const Todo = () => import('../views/todo/todo.vue')
// import Login from '../views/login/login.vue'
const Login = () => import('../views/login/login.vue')

export default [
    {
        path: '/',
        redirect: '/app'
    },
    {
        // path: '/app', // /app/xxx
        path: '/app/:id', // /app/xxx
        props: true,
        component: Todo,
        // 路由懒加载
        // component: () => import('../views/todo/todo.vue'),
        // components: {
        //     default: Todo,
        //     a: Login
        // },
        name: 'app',
        meta: {
            title: 'this is app',
            description: 'asdasd'
        },
        // 路由独享的守卫 -> 组件守卫案例在TODO组件中
        beforeEnter (to, from, next) {
            console.log('app route before enter')
            next()
        }
        // children: [
        //     {
        //         path: 'test',
        //         component: Login
        //     }
        // ]
    },
    {
        path: '/login',
        component: Login
        // components: {
        //     default: Login,
        //     a: Todo
        // }
    }
]
