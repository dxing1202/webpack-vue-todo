import Vue from 'vue'
import App from './app.vue'
// 引入Vuex
import Vuex from 'vuex'

import './assets/styles/global.styl'

// const root = document.createElement('div')
// document.body.appendChild(root)

// new Vue({
//     render: (h) => h(App)
// }).$mount(root);

// 使用router插件后不需要上面这种引入

import VueRouter from 'vue-router'

import createRouter from './config/router'
import createStore from './store/store'

Vue.use(VueRouter)
Vue.use(Vuex)

const router = createRouter()
const store = createStore()

// 导航守卫

// router.beforeEach 全局前置守卫
// 可以用在场景登录页面
router.beforeEach( (to, from, next) => {
    console.log('before each invoked')
    // console.log(to)
    // if (to.fullPath === '/app') {
    //     next('/login')
    // } else {
    //     next()
    // }
    next()
})

// router.beforeResolve 全局解析守卫
router.beforeResolve( (to, from, next) => {
    console.log('before resolve invoked')
    next()
})

router.afterEach( (to, from) => {
    console.log('after each invoked')
})

new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount("#app");
