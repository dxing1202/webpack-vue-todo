export default {
    updateCountAsync (store, data) {
        setTimeout( () => {
            // store.commit('updateCount', data.num)
            store.commit('updateCount', {
                num: data.num
            })
        }, data.time)
    }
}
