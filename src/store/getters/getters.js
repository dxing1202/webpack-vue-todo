export default {
    fullName (state) {
        // 老师使用的格式， 老版本的样式
        // return '${state.firstName} ${state.lastName}'

        // 官网格式
        return state.firstName + ' ' + state.lastName
    }
}
