import Vuex from 'vuex'
// import Vue from 'vue'
// Vue.use(Vuex)

// const store = new Vuex.Store({
//     state: {
//         count: 0
//     },
//     mutations: {
//         // 老师写法
//         updateCount (state, num) {
//             state.count = num
//         }
//     }
// })

// export default store

import defaultState from './state/state'
import mutations from './mutations/mutations'
import getters from './getters/getters'
import actions from './actions/actions'

const isDev = process.env.NODE_ENV === 'development'

// 老师建议使用这种返回的做法，可以避免溢出内存
export default () => {
    return new Vuex.Store({
        // 严格模式 这样就不可以在外面修改store.state
        // 不推荐在部署模式使用 部署模式下需要关闭
        // strict: true,
        strict: isDev,
        // state: {
        //     count: 0
        // },
        state: defaultState,
        // mutations: {
        //     // 老师写法
        //     updateCount (state, num) {
        //         state.count = num
        //     }
        // }
        // mutations: mutations,
        mutations,
        // getters: getters
        getters,
        // actions: actions
        actions
    })
}
